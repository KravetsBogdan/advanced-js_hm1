class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value){
        this._name = value;
    };
    get name(){
        return this._name;
    }

    set age(value){
        this._age = value;
    };
    get age(){
        return this._age;
    }

    set salary(value){
        this._salary = value;
    };
    get salary(){
        return this._salary;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }

    get salary(){
        return this._salary * 3;
    }
}

const programmer = new Programmer('Bohdan', 20, 10, ['ua','en']);
const programmer2 = new Programmer('Egor', 22, 30, ['ua', 'en', 'esp']);
const programmer3 = new Programmer('Slava', 30, 60, ['ua']);


console.log(programmer);
console.log(programmer2);
console.log(programmer3);